
## Installazione del sito

```
cd websites
drush qc --domain="testspid7.dev"
rm -rf testspid7.dev
git clone https://infora_sh@bitbucket.org/infora_sh/testspid7.dev.git
... Caricare l'ultimo db in /home/drupalpro/websites/testspid7.dev/sites/default/files_private/backup_migrate/manual/
...   in testspid7_dev. Il sito � funzionante.
```

## Installazione di Samlphp

```
sudo bash
cd /var
wget https://simplesamlphp.org/download?latest simplesamlphp.tar.gz
tar xzf simplesamlphp
cd simplesamlphp
cp ~/websites/testspid7.dev/materiale/config.php /var/simplesamlphp/config/config.php 
cp ~/websites/testspid7.dev/materiale/testspid7.dev.conf /etc/apache2/sites-available/testspid7.dev.conf 

sudo apt-get -y update 
sudo apt-get install php7.0-ldap -y		# Estensione per l'LDAP login
sudo apt-get install memcached -y		# Installazione di Memcached
sudo apt-get install php-memcached -y	# Attivazione della PHP Extension
sudo service apache2 restart
```

Visitare la pagina: http://testspid7.dev/simplesaml/module.php/core/frontpage_config.php

La password di accesso � "admin"

## Staging

Il sito è installato su http://spid.arroddug.org

Il .conf si trova su materiale/testspid7.dev.arroddu.conf

Per eliminare il problema del login rotto, causato da $_simplesamlphp_auth_as->isAuthenticated()
chiamare http://spid.arroddug.org/user?spid=disable e fare il login (la disabilitazione è solo temporanea)
